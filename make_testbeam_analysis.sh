#!/bin/sh

code=TestbeamAnalysis.cxx
exec=testbeam_analysis.exe
rm -f $exec

CXX=$($ROOTSYS/bin/root-config --cxx)
flags="$($ROOTSYS/bin/root-config --cflags --glibs) -lTreePlayer -Iatlasstyle"

success=no
$CXX $flags $code -o $exec && {
    echo; echo "  Compilation successful!"; echo;
    success=yes
} || {
    echo ; echo "Compilation failed :("
}
echo "------------"

[[ $success = yes ]] && {
    echo ; echo "  Running..."; echo;
    ./$exec $1 $2 $3 $4 $5

    echo ; echo "------------"
}
