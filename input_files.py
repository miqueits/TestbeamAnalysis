#!/bin/python

# path for output files
output_path = "./plots/"

# path for input files
input_path = "./pos1/"

# thresholds
thresholds = [30,45,60,75,85,95,105,115,125,150,165,180]

# input files
files = ["TestbeamAnalysis_2767_pos1_30_500.root",
"TestbeamAnalysis_2768_pos1_45_500.root",
"TestbeamAnalysis_2769_2770_pos1_60_500.root",
"TestbeamAnalysis_2771_2772_2774_pos1_75_500.root",
"TestbeamAnalysis_2776_2777_pos1_85_500.root",
"TestbeamAnalysis_2778_2779_2860_pos1_95_500.root",
"TestbeamAnalysis_2780_2862_pos1_105_500.root",
"TestbeamAnalysis_2781_2863_2864_2865_pos1_115_500.root",
"TestbeamAnalysis_2782_pos1_125_500.root",
"TestbeamAnalysis_2788_2866_pos1_150_500.root",
"TestbeamAnalysis_2867_2868_pos1_165_500.root",
"TestbeamAnalysis_2869_2871_pos1_180_500.root"]
